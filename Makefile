NAME = ft_ls

SRCDIR = src
OBJDIR = objects
INCLUDE = include
LIBDIR = libft
LIB = ft
CFLAGS = -Wall -Werror -Wextra -I $(INCLUDE) 
CC = gcc

SRCFILES = main.c					\
		   clean_funcs.c			\
		   collect_info.c			\
		   collect_info_utils.c		\
		   collect_info_utils_1.c	\
		   errors.c					\
		   format_output.c			\
		   format_output_utils.c	\
		   parse_args.c				\
		   parse_args_utils.c		\
		   process_dir.c			\
		   process_dir_utils.c		\
		   q_sort.c					\
		   queue.c					\
		   utils.c					

OBJFILES = $(SRCFILES:.c=.o)

all: $(NAME)

$(NAME): $(LIB) $(addprefix $(OBJDIR)/, $(OBJFILES))
	$(CC) $(CFLAGS) $(addprefix $(OBJDIR)/, $(OBJFILES)) -L $(LIBDIR) -l$(LIB) -o $@

$(OBJDIR):
	mkdir $@

$(LIB):
	make -C $(LIBDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(OBJDIR)
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	make clean -C $(LIBDIR)
	rm -rf $(OBJDIR)

fclean: clean
	make fclean -C $(LIBDIR)
	rm -f $(NAME)

re: fclean all
