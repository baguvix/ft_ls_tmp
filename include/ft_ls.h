/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/28 13:33:58 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 03:04:08 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <dirent.h>
# include <unistd.h>
# include <stdio.h>
# include <time.h>
# include <sys/xattr.h>
# include <sys/types.h>
# include <sys/dir.h>
# include <sys/stat.h>
# include <sys/acl.h>
# include <sys/ioctl.h>
# include <pwd.h>
# include <uuid/uuid.h>
# include <grp.h>
# include <errno.h>
# include "libft.h"
# include "ft_printf.h"

# define ISSET(x, y)	((x) & (1 << (y)))
# define MAJOR(x)		((int)(((unsigned int)(x) >> 24)&0xff))
# define MINOR(x)		((int)((x)&0xff))
# define FLAGS			"lratRufgdGC1SA"
# define PRINT_DIR_NAME	(1ULL << (sizeof(unsigned long long) * 8 - 1))
# define INPUT			(1ULL << (sizeof(unsigned long long) * 8 - 2))
# define BUFF_SIZE		1024

enum						e_flags
{
	L = 0, R, A, T, BIG_R, U, F, G,
	D, BIG_G, BIG_C, ONE, S, BIG_A
};

typedef enum				e_mode
{
	ALPHA, TIME, UTIME, SIZE
}							t_mode;

typedef unsigned long long	t_exec_flags;

int							g_status;

typedef struct				s_dir_entry
{
	char				*gname;
	char				*uname;
	char				*fname;
	char				*path;
	char				*time;
	struct stat			stat;
	char				rwx[12];
	struct s_dir_entry	*next;
}							t_dir_entry;

typedef struct				s_direnv
{
	t_exec_flags	f;
	nlink_t			link_f;
	int				uname_f;
	int				gname_f;
	off_t			bytes_f;
	int				fname_f;
	int				maj_f;
	int				min_f;
	int				line_size;
	size_t			chunks;
}							t_direnv;

typedef struct				s_queue
{
	t_dir_entry		*data;
	struct s_queue	*next;
}							t_queue;

void						q_sort(t_dir_entry **arr, size_t len, t_mode mode);
int							enqueue(t_queue **q, t_dir_entry *data);
t_dir_entry					*dequeue(t_queue **q);
int							del_queue(t_queue **q);
void						clear_entry(t_dir_entry **ent);
void						clear_list(t_dir_entry **lst);
void						clear_arr(t_dir_entry **arr, int len);
void						del_unnecessary(t_dir_entry **arr,
							int len, t_exec_flags flags);
void						*print_error(char *ent);
void						init_exit(int status);
int							get_flag_id(char flag);
void						set_bit(t_exec_flags *flags, int id);
void						unset_bit(t_exec_flags *flags, int id);
void						flags_priority(t_exec_flags *flags, int id);
void						get_opt(char *optstr, t_exec_flags *flags);
t_dir_entry					*init_input(char *av);
int							skip_check(char *av);
int							read_flags(int ac, char **av, t_exec_flags *flags);
t_dir_entry					*read_args(int ac, char **av, t_exec_flags *flags);
int							new_hope(t_dir_entry *e);
int							get_ugid(t_dir_entry *arr, t_exec_flags f);
char						file_type(mode_t mode);
char						special_perm(mode_t	mode, mode_t x);
char						alternate_access(t_dir_entry *entry);
int							get_rwx(t_dir_entry *arr);
char						*get_sym(t_dir_entry *e);
int							skip_check(char *av);
int							skip2(t_dir_entry *dir,
							char *fname, t_exec_flags f);
void						sort_arbiter(t_dir_entry **arr,
							ssize_t len, t_exec_flags f);
size_t						multicol(t_direnv *d, t_exec_flags f);
int							buffer_mash(t_dir_entry **arr,
							ssize_t len, t_direnv *d);
int							col_form(t_dir_entry **arr,
							ssize_t len, t_direnv *d);
int							print_info(t_dir_entry **arr, ssize_t len,
							t_direnv *env, t_exec_flags flags);
size_t						get_direnv(t_dir_entry **arr, ssize_t len,
							t_direnv *d, t_exec_flags f);
void						aux_params(ssize_t len, t_dir_entry **arr,
							t_direnv *d, t_exec_flags f);
int							process_dir(t_dir_entry *dir, t_exec_flags flags);
t_dir_entry					*init_entry(t_dir_entry *dir, struct dirent *tmp);
t_dir_entry					*scan_dir(t_dir_entry *dir, t_exec_flags flags);
t_queue						*create_queue(t_dir_entry **ent, ssize_t len,
							t_exec_flags flags);
int							gen_info(t_dir_entry **arr, ssize_t len,
							const t_exec_flags f);
int							update(t_dir_entry **arr,
							ssize_t len, t_exec_flags f);
void						reverse(t_dir_entry **arr, ssize_t len);
ssize_t						list_to_arr(t_dir_entry *list, t_dir_entry ***arr);
int							file_vane(t_dir_entry *e, t_exec_flags f);
int							exclude_dirs(t_dir_entry **files,
							t_dir_entry **head, t_exec_flags f);
int							cast_lstat(t_dir_entry **arr, ssize_t len);
int							get_time(t_dir_entry *entry, t_exec_flags f);
void						sort_inp(char **av, size_t low, size_t high);
void						*safe_exit_scan_dir(t_dir_entry **head, DIR *dirp);
int							exit_condition(void);
void						kill_dir(char **av, int ac);
void						q_sort(t_dir_entry **arr, size_t len, t_mode mode);
void						entry_swap(t_dir_entry **arr, size_t i, size_t j);
size_t						partition_utime(t_dir_entry **arr,
							size_t low, size_t high);
size_t						partition_time(t_dir_entry **arr,
							size_t low, size_t high);
size_t						partition_alpha(t_dir_entry **arr,
							size_t low, size_t high);
size_t						partition_size(t_dir_entry **arr,
							size_t low, size_t high);
void						q_sort_generic(t_dir_entry **arr,
							size_t low, size_t high,
							size_t (*part)(t_dir_entry **, size_t, size_t));
#endif
