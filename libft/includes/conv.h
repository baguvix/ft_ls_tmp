/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conv.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 13:01:23 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/19 15:13:57 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONV_H
# define CONV_H

# include "ft_printf.h"

ssize_t	c_conv(t_spec *spec, char **rtu);
ssize_t	s_conv(t_spec *spec, char **rtu);
ssize_t	bs_conv(t_spec *spec, char **rtu);
ssize_t	fe_conv(t_spec *sanya, char **rtu);
ssize_t	diu_conv(t_spec *spec, char **rtu);
ssize_t	o_conv(t_spec *spec, char **rtu);
ssize_t	x_conv(t_spec *spec, char **rtu);
ssize_t	p_conv(t_spec *spec, char **rtu);

t_conv	choose_conv(t_spec *spec);

#endif
