/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftoa_utils.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 21:22:07 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/18 14:46:58 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FP_CORE_H
# define FP_CORE_H

# include "libft.h"
# include "utils.h"

# define EXP_BIAS_NORMAL	16383
# define EXP_BIAS_DENORMAL	16382
# define POWER_BITS_ALL_ONE	32767
# define DEFAULT_SIZE		300
# define SIGN_OFF			(1ULL << 15)
# define DIGIT_MASK			1000000000000000000
# define LOG2_10			3.3219280948874

typedef unsigned long long	t_ull;

typedef struct				s_fp_parts
{
	t_ull					low;
	t_ull					high;
}							t_fpp;

typedef union				u_fp
{
	long double				f;
	t_fpp					df;
}							t_fp;

typedef struct				s_big_num
{
	t_fp					a;
	size_t					len;
	t_ull					*p;
	int						pow;
	char					sign;
}							t_bn;

typedef struct				s_e
{
	char					pow_sign;
	unsigned int			pow;
}							t_e;

int							mul_2(t_bn *n);
int							div_2(t_bn *n);
void						num_map(t_bn *n, char *s, size_t str_len);
void						correction(char *s, size_t len);
char						*e_help(t_e *e, char *stmp);
int							gen_num_neg(t_bn *n, t_spec *sanya, char **s);
int							gen_num_nneg(t_bn *n, t_spec *sanya, char **s);
ssize_t						wrap_up(t_spec *s, char **rtu);
ssize_t						naninf(t_bn *n, t_spec *sanya, char **rtu);
ssize_t						ftoa(t_spec *sanya, char **rtu);

#endif
