/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   auto_string.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 12:56:19 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/11 11:37:12 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AUTO_STRING_H
# define AUTO_STRING_H
# define OUT_STR_SIZE 128

# include <string.h>

typedef	struct
{
	size_t	size;
	size_t	remained_space;
	char	*content;
}	t_auto_str;

char		*str_init(t_auto_str *str);
char		*str_extend(t_auto_str *str);
char		*str_add(t_auto_str *str, char *s, size_t len);

#endif
