/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:55:28 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:55:30 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t	i;

	if (s1 == NULL || s2 == NULL)
		return (0);
	if (!n)
		return (1);
	i = 0;
	while (s1[i] && (s1[i] == s2[i]) && --n)
		++i;
	if (s2[i] == s1[i])
		return (1);
	return (0);
}
