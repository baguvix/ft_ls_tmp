/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:57:10 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:57:12 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	char	*nd;
	char	*c_p;

	if (!(*needle))
		return ((char*)haystack);
	if (len == 0)
		return (NULL);
	while (*haystack && len)
	{
		if (*haystack == *needle)
		{
			c_p = (char*)haystack;
			nd = (char*)needle;
			while (*nd && len-- && (*haystack++ == *nd))
				++nd;
			if (*nd == '\0')
				return (c_p);
			len += haystack - c_p;
			haystack = c_p;
		}
		++haystack;
		--len;
	}
	return (NULL);
}
