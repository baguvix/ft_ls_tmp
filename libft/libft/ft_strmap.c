/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:51:49 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:51:50 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*new;
	size_t	len;

	if (!(s && f))
		return (NULL);
	new = (char*)(s);
	while (*new)
		++new;
	len = new - s;
	if ((new = (char*)malloc(len + 1)) == NULL)
		return (NULL);
	while (*s)
		*new++ = f(*s++);
	*new = '\0';
	return (new - len);
}
