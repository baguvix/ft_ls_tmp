/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/18 14:21:44 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/18 14:22:27 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnchr(const char *s, int c, size_t n)
{
	int i;

	i = 0;
	while (s[i] && n--)
	{
		if (s[i] == (char)c)
			return ((char *)s + i);
		i++;
	}
	return (s[i] == (char)c ? (char *)s + i : 0);
}
