/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nofd.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/13 16:20:02 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/13 16:24:03 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		nofd(unsigned long long a, short base)
{
	size_t	count;

	count = 1;
	a /= base;
	while (a && ++count)
		a /= base;
	return (count);
}
