/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:58:26 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:58:27 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	char	*c_p;
	char	*nd;

	if (!(*needle))
		return ((char*)haystack);
	while (*haystack)
	{
		if (*haystack == *needle)
		{
			c_p = (char*)haystack;
			nd = (char*)needle;
			while (*nd && (*haystack++ == *nd))
				++nd;
			if (*nd == '\0')
				return (c_p);
			haystack = c_p;
		}
		++haystack;
	}
	return (NULL);
}
