/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:57:26 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:57:28 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char	const *s, unsigned int start, size_t len)
{
	char	*sub;
	char	*tmp;

	if (s == NULL)
		return (NULL);
	if ((sub = (char*)malloc(len + 1)) == NULL)
		return (NULL);
	tmp = sub;
	while (len--)
		*tmp++ = *(s + start++);
	*tmp = '\0';
	return (sub);
}
