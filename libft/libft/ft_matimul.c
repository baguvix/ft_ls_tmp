/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_matimul.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/03 16:18:13 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/03 16:20:21 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_mati	*ft_matimul(t_mati *a, t_mati *b)
{
	t_mati	*new;
	size_t	i;
	size_t	j;
	size_t	k;

	if (!a || !b || (a->n != b->m))
		return (NULL);
	i = -1;
	if ((new = ft_create_mati(a->m, b->n)))
		while (++i < a->m)
		{
			j = -1;
			while (++j < b->n)
			{
				k = -1;
				while (++k < a->n)
					new->elem[i * b->n + j] +=
						a->elem[i * a->n + k] * b->elem[k * b->n + j];
			}
		}
	return (new);
}
