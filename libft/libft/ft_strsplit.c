/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 04:02:34 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/03 18:28:25 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*assistant(char const *s, char c, t_list **words)
{
	char	*end;
	char	*mas;
	size_t	w_len;

	if (!(end = ft_strchr(s, c)))
		end = ft_strchr(s, '\0');
	w_len = end - --s;
	if (!(mas = (char*)malloc((w_len + 1) * sizeof(char))))
	{
		ft_lstdel(words, ft_deldata);
		return (NULL);
	}
	(mas)[w_len] = '\0';
	ft_strncpy(mas, s, w_len);
	ft_lstadd(words, ft_lstnew(mas, w_len + 1));
	free(mas);
	return (end);
}

char		**ft_strsplit(char const *s, char c)
{
	size_t	m_len;
	char	**mas;
	t_list	*words;

	if (!s)
		return (NULL);
	m_len = 0;
	words = NULL;
	while (*s)
		if (*s++ != c)
		{
			++m_len;
			if (!(s = assistant(s, c, &words)))
				return (NULL);
		}
	if (!(mas = (char**)malloc(sizeof(char*) * (m_len + 1))))
	{
		ft_lstdel(&words, ft_deldata);
		return (NULL);
	}
	mas[m_len] = NULL;
	while (m_len--)
		mas[m_len] = (char*)(ft_lstpicktop(&words));
	return (mas);
}
