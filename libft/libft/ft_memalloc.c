/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:41:08 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:41:11 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	char	*mem;
	char	*tmp;

	if ((mem = (char*)malloc(size)) == NULL)
		return (NULL);
	tmp = mem;
	while (size--)
		*tmp++ = '\0';
	return ((void*)(mem));
}
