/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstget_i.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/12 18:01:26 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/09 13:23:19 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstget_i(t_list *head, size_t index)
{
	size_t i;

	if (!head)
		return (0);
	i = 0;
	while (head && i < index)
	{
		head = head->next;
		i++;
	}
	return (head);
}
