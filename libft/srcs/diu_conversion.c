/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   diu_conversion.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 21:37:20 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/21 22:01:43 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"
#include "utils.h"

static char		*get_number(unsigned long long a, int prec, int base)
{
	char	*res;
	int		i;
	int		tmp;

	tmp = nofd(a, base);
	if ((prec <= 0 || prec < tmp) && !(a == 0 && prec == 0))
		prec = tmp;
	if ((res = (char *)malloc(prec + 1)) == NULL)
		return (NULL);
	i = prec;
	while (i-- > 0)
	{
		res[i] = a % base + '0';
		a /= base;
	}
	res[prec] = '\0';
	return (res);
}

ssize_t			diu_conv(t_spec *s, char **rtu)
{
	int		len;
	char	*num;
	size_t	num_len;

	if (s->type == 'u')
		s->sign = '\0';
	if ((num = get_number(s->value.ll, s->prec, 10)) == NULL)
		return (-1);
	if (s->prec != -1 || s->align_left)
		s->filling = ' ';
	num_len = ft_strlen(num);
	len = num_len + (s->sign ? 1 : 0);
	len = (s->width < len ? len : s->width);
	if ((*rtu = (char *)malloc(len + 1)) == NULL)
		return (-1);
	(*rtu)[len] = '\0';
	ft_memset(*rtu, s->filling, len);
	len = len - num_len;
	if (s->align_left)
		len = (s->sign ? 1 : 0);
	ft_memcpy(*rtu + len, num, num_len);
	if (s->sign)
		(*rtu)[len - (s->filling == '0' ? len : 1)] = s->sign;
	free(num);
	return (ft_strlen(*rtu));
}
