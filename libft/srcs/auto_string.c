/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   auto_string.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 13:44:57 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/09 18:43:08 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "auto_string.h"
#include "libft.h"

char	*str_add(t_auto_str *str, char *s, size_t len)
{
	while (str->remained_space < len)
		if ((str->content = str_extend(str)) == NULL)
			return (NULL);
	ft_memcpy(str->content + str->size - str->remained_space, s, len);
	str->remained_space -= len;
	return (str->content);
}

char	*str_extend(t_auto_str *str)
{
	char	*tmp;

	tmp = ft_strnew(str->size * 2);
	if (tmp)
	{
		ft_memcpy(tmp, str->content, str->size - str->remained_space);
		str->remained_space += str->size;
		str->size += str->size;
		free(str->content);
	}
	return (tmp);
}

char	*str_init(t_auto_str *str)
{
	str->size = OUT_STR_SIZE;
	str->remained_space = str->size;
	return ((str->content = ft_strnew(str->size * sizeof(char))));
}
