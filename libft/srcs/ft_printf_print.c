/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_print.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/10 18:25:26 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/20 14:10:18 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_print.h"
#include "conv.h"
#include "auto_string.h"

static ssize_t	colorize(t_spec *spec, char **rtu, size_t size)
{
	char	*res;
	size_t	ret;

	if (!rtu)
		return (-1);
	ret = size + sizeof("\x1b[31m") + sizeof("\x1b[0m") - 2;
	if ((res = (char *)malloc(ret)) == 0)
	{
		free(*rtu);
		return (-1);
	}
	ft_memcpy(res, "\x1b[", 5);
	res[2] = spec->color ? '3' : '4';
	res[3] = '0' + (spec->color ? spec->color : spec->bgcolor) - 1;
	res[4] = 'm';
	ft_memcpy(res + 5, *rtu, size);
	ft_memcpy(res + 5 + size, "\x1b[0m", 4);
	free(*rtu);
	*rtu = res;
	spec->color = 0;
	return (ret);
}

static ssize_t	set_color(t_spec *spec, char **rtu, ssize_t size)
{
	char	prev_color;

	if (spec->color)
		if ((size = colorize(spec, rtu, size)) < 0)
			return (-1);
	prev_color = spec->color;
	spec->color = 0;
	if (spec->bgcolor)
		if ((size = colorize(spec, rtu, size)) < 0)
			return (-1);
	spec->color = prev_color;
	return (size);
}

static int		do_conv(t_auto_str *str, t_spec *spec)
{
	t_conv	f;
	ssize_t	ret;
	char	*rtu;

	f = choose_conv(spec);
	if ((ret = f(spec, &rtu)) < 0)
	{
		free(str->content);
		return (-1);
	}
	if (spec->color || spec->bgcolor)
		if ((ret = set_color(spec, &rtu, ret)) < 0)
		{
			free(str->content);
			return (-1);
		}
	if (str_add(str, rtu, ret) == 0)
	{
		free(str->content);
		ret = -1;
	}
	free(rtu);
	return (ret);
}

int				print_to_fd(int fd, t_list *spec)
{
	int			res;
	t_auto_str	str;
	ssize_t		ret;

	res = 0;
	if (str_init(&str) == 0)
		return (-1);
	while (spec)
	{
		if ((ret = do_conv(&str, spec->content)) < 0)
			return (-1);
		res += ret;
		spec = spec->next;
	}
	write(fd, str.content, res);
	free(str.content);
	return (res);
}

int				print_to_str(char *res_str, t_list *spec)
{
	int			res;
	t_auto_str	str;
	ssize_t		ret;

	res = 0;
	if (str_init(&str) == 0)
		return (-1);
	while (spec)
	{
		if ((ret = do_conv(&str, spec->content)) < 0)
			return (-1);
		res += ret;
		spec = spec->next;
	}
	ft_memcpy(res_str, str.content, res);
	free(str.content);
	return (res);
}
