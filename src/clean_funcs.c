/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_funcs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 20:03:33 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:25:43 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	clear_entry(t_dir_entry **ent)
{
	t_dir_entry	*bye;

	if ((bye = *ent) != NULL)
	{
		free(bye->fname);
		free(bye->path);
		free(bye->time);
		free(bye->uname);
		free(bye->gname);
		free(bye);
		*ent = NULL;
	}
}

void	clear_list(t_dir_entry **lst)
{
	if (*lst != NULL)
	{
		if ((*lst)->next)
			clear_list(&(*lst)->next);
		clear_entry(lst);
	}
}

void	clear_arr(t_dir_entry **arr, int len)
{
	while (len--)
		clear_entry(arr + len);
}

void	del_unnecessary(t_dir_entry **arr, int len, t_exec_flags flags)
{
	if (len > 0)
		while (len--)
			if ((arr[len]->stat.st_mode & S_IFDIR) == 0 || !ISSET(flags, BIG_R))
				clear_entry(arr + len);
}

void	*safe_exit_scan_dir(t_dir_entry **head, DIR *dirp)
{
	clear_list(head);
	closedir(dirp);
	return (NULL);
}
