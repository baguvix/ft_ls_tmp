/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:36:08 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:36:12 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	set_bit(t_exec_flags *flags, int id)
{
	*flags |= (1 << id);
}

void	unset_bit(t_exec_flags *flags, int id)
{
	*flags &= ~(1 << id);
}

int		skip_check(char *av)
{
	struct stat	tmp;

	lstat(av, &tmp);
	if (errno)
	{
		if (errno == ENOENT || errno == EPERM || errno == EACCES)
			print_error(av);
		errno = 0;
		return (1);
	}
	return (0);
}

int		skip2(t_dir_entry *dir, char *fname, t_exec_flags f)
{
	struct stat	t;
	char		*str;

	if ((!ft_strcmp(".", fname) || !ft_strcmp("..", fname)) && ISSET(f, BIG_A))
		return (1);
	if (ft_strstr(dir->path, "dev/fd/") || (*fname == '.' && (!ISSET(f, A)
		&& !ISSET(f, BIG_A))))
		return (1);
	if (ISSET(f, L) || ISSET(f, T) || ISSET(f, BIG_R))
	{
		str = ft_dstrcat(NULL, dir->path);
		str = ft_dstrcat(str, fname);
		errno = 0;
		lstat(str, &t);
		free(str);
		if (errno == EPERM || errno == EACCES)
			return (1);
	}
	return (0);
}

ssize_t	list_to_arr(t_dir_entry *list, t_dir_entry ***arr)
{
	t_dir_entry	*tmp;
	ssize_t		len;

	tmp = list;
	len = 0;
	while (tmp)
	{
		++len;
		tmp = tmp->next;
	}
	if ((*arr = (t_dir_entry **)malloc(sizeof(t_dir_entry *) * len)) == 0)
		return (-1);
	len = 0;
	while (list)
	{
		(*arr)[len++] = list;
		list = list->next;
	}
	return (len);
}
