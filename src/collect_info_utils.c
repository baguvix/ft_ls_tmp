/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   collect_info_utils.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:13:56 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:14:56 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_ugid(t_dir_entry *arr, t_exec_flags f)
{
	struct passwd	*pass;
	struct group	*grp;

	errno = 0;
	if (ISSET(f, G))
		arr->uname = ft_dstrcat(NULL, "");
	else
	{
		if ((pass = getpwuid(arr->stat.st_uid)) == NULL && errno != 0)
			return (-1);
		arr->uname = ft_dstrcat(NULL, pass->pw_name);
	}
	if ((grp = getgrgid(arr->stat.st_gid)) == NULL && errno != 0)
		return (-1);
	arr->gname = ft_dstrcat(NULL, grp->gr_name);
	if (!arr->uname || !arr->gname)
		return (-1);
	return (0);
}

char	file_type(mode_t mode)
{
	mode &= S_IFMT;
	if (S_ISFIFO(mode))
		return ('p');
	if (S_ISCHR(mode))
		return ('c');
	if (S_ISDIR(mode))
		return ('d');
	if (S_ISBLK(mode))
		return ('b');
	if (S_ISREG(mode))
		return ('-');
	if (S_ISLNK(mode))
		return ('l');
	if (S_ISSOCK(mode))
		return ('s');
	if (S_ISWHT(mode))
		return ('w');
	return ('?');
}

char	special_perm(mode_t mode, mode_t x)
{
	if ((mode & S_ISGID && x == S_IXGRP) || (mode & S_ISUID && x == S_IXUSR))
	{
		if (mode & x)
			return ('s');
		return ('S');
	}
	if (mode & S_ISVTX && x == S_IXOTH)
	{
		if (mode & x)
			return ('t');
		return ('T');
	}
	if (mode & x)
		return ('x');
	return ('-');
}

char	alternate_access(t_dir_entry *entry)
{
	acl_t	acl;
	ssize_t	rc;

	errno = 0;
	if (entry->rwx[0] == 'c' || entry->rwx[0] == 'b')
		return ('\0');
	if ((rc = listxattr(entry->path, NULL, 0, XATTR_NOFOLLOW)) > 0)
		return ('@');
	else if (rc < 0 && errno != ENOENT && errno != EACCES)
		return (-1);
	acl = acl_get_link_np(entry->path, ACL_TYPE_EXTENDED);
	if (acl)
		if (acl_free(acl) == 0)
			return ('+');
	if (errno && errno != ENOENT && errno != EACCES)
		return (-1);
	return ('\0');
}

int		get_rwx(t_dir_entry *arr)
{
	arr->rwx[0] = file_type(arr->stat.st_mode);
	arr->rwx[1] = (arr->stat.st_mode & S_IRUSR) ? 'r' : '-';
	arr->rwx[2] = (arr->stat.st_mode & S_IWUSR) ? 'w' : '-';
	arr->rwx[3] = special_perm(arr->stat.st_mode, S_IXUSR);
	arr->rwx[4] = (arr->stat.st_mode & S_IRGRP) ? 'r' : '-';
	arr->rwx[5] = (arr->stat.st_mode & S_IWGRP) ? 'w' : '-';
	arr->rwx[6] = special_perm(arr->stat.st_mode, S_IXGRP);
	arr->rwx[7] = (arr->stat.st_mode & S_IROTH) ? 'r' : '-';
	arr->rwx[8] = (arr->stat.st_mode & S_IWOTH) ? 'w' : '-';
	arr->rwx[9] = special_perm(arr->stat.st_mode, S_IXOTH);
	arr->rwx[10] = alternate_access(arr);
	arr->rwx[11] = '\0';
	if (arr->rwx[10] == -1)
		return (-1);
	return (0);
}
