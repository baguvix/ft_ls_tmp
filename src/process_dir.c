/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_dir.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:16:36 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:17:17 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

size_t			process_setup(t_dir_entry *tmp, t_queue **queue, t_exec_flags f)
{
	t_dir_entry	**arr;
	t_direnv	d;
	ssize_t		len;

	len = list_to_arr(tmp, &arr);
	if (len > 0 && gen_info(arr, len, f) < 0)
		len = -1;
	ft_memset(&d, 0, sizeof(t_direnv));
	d.line_size = get_direnv(arr, len, &d, f);
	if (len > 0 && (*queue = create_queue(arr, len, f)) == NULL)
		if (errno != 0)
			len = -1;
	if (len > 0 && ISSET(f, L))
		ft_printf("total %zd\n", d.chunks);
	if (len > 0 && print_info(arr, len, &d, f) < 0)
		len = -1;
	del_unnecessary(arr, len, f);
	free(arr);
	return (len);
}

int				process_dir(t_dir_entry *dir, t_exec_flags flags)
{
	t_dir_entry		*tmp;
	t_queue			*queue;
	ssize_t			e;

	errno = 0;
	if ((tmp = scan_dir(dir, flags)) == NULL)
		return (exit_condition());
	e = process_setup(tmp, &queue, flags);
	flags |= PRINT_DIR_NAME;
	while (e > 0 && (tmp = dequeue(&queue)) && write(1, "\n", 1))
	{
		if (process_dir(tmp, flags) < 0)
		{
			del_queue(&queue);
			e = -2;
		}
		else
			clear_entry(&tmp);
	}
	if (e == -1)
		clear_list(&tmp);
	return (e);
}

t_dir_entry		*init_entry(t_dir_entry *dir, struct dirent *tmp)
{
	t_dir_entry		*ent;

	ent = (t_dir_entry *)malloc(sizeof(t_dir_entry));
	if (ent == NULL)
		return (NULL);
	ft_memset(ent, 0, sizeof(t_dir_entry));
	ent->path = ft_dstrcat(ft_strdup(dir->path), tmp->d_name);
	ent->fname = ft_dstrcat(NULL, tmp->d_name);
	return (ent);
}

t_dir_entry		*scan_dir(t_dir_entry *dir, t_exec_flags flags)
{
	DIR				*dirp;
	struct dirent	*tmp;
	t_dir_entry		*head;
	t_dir_entry		*entry;

	if (flags & PRINT_DIR_NAME)
		ft_printf("%.*s:\n", (!ft_strcmp(dir->path, "/")
						? 1 : ft_strlen(dir->path) - 1), dir->path);
	if ((dirp = opendir(dir->path)) == NULL)
		return ((t_dir_entry *)print_error(dir->fname));
	head = NULL;
	while ((tmp = readdir(dirp)) != NULL)
	{
		if (skip2(dir, tmp->d_name, flags))
			continue;
		if ((entry = init_entry(dir, tmp)) == NULL)
			return (safe_exit_scan_dir(&head, dirp));
		entry->next = head;
		head = entry;
	}
	closedir(dirp);
	return (head);
}

t_queue			*create_queue(t_dir_entry **ent, ssize_t len,
							t_exec_flags flags)
{
	t_queue	*queue;
	ssize_t	i;

	errno = 0;
	queue = NULL;
	i = -1;
	if (ISSET(flags, BIG_R))
		while (++i < len)
			if (S_ISDIR(ent[i]->stat.st_mode)
					&& ft_strcmp(ent[i]->fname, ".") != 0
					&& ft_strcmp(ent[i]->fname, "..") != 0)
				if (enqueue(&queue, ent[i]) < 0)
					del_queue(&queue);
	return (queue);
}
