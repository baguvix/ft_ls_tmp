/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_args_utils.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:34:07 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:34:37 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int			get_flag_id(char flag)
{
	char	*flags;
	int		i;

	flags = FLAGS;
	i = 0;
	while (flags[i] && flags[i] != flag)
		++i;
	if (flags[i])
		return (i);
	return (-1);
}

void		flags_priority(t_exec_flags *flags, int id)
{
	if (id == L)
	{
		unset_bit(flags, ONE);
		unset_bit(flags, BIG_C);
		unset_bit(flags, G);
	}
	if (id == ONE)
	{
		unset_bit(flags, BIG_C);
		unset_bit(flags, G);
		unset_bit(flags, L);
	}
	if (id == BIG_C)
	{
		unset_bit(flags, ONE);
		unset_bit(flags, G);
		unset_bit(flags, L);
	}
	if (id == G)
	{
		unset_bit(flags, ONE);
		unset_bit(flags, BIG_C);
		unset_bit(flags, L);
	}
}

void		get_opt(char *optstr, t_exec_flags *flags)
{
	int		id;
	int		maxn_flags;
	char	*tmp;

	tmp = optstr;
	maxn_flags = sizeof(t_exec_flags) * 8;
	while (*++tmp)
	{
		if ((id = get_flag_id(*tmp)) != -1 && id < maxn_flags)
		{
			set_bit(flags, id);
			flags_priority(flags, id);
		}
		else
		{
			ft_dprintf(2, "ls: illegal option -- %c\n", *tmp);
			ft_dprintf(2, "%s%s\n",
				"usage: ls [-ABCFGHLOPRSTUW",
				"abcdefghiklmnopqrstuwx1] [file ...]");
			exit(1);
		}
	}
}

t_dir_entry	*init_input(char *av)
{
	t_dir_entry	*input;

	if ((input = (t_dir_entry *)malloc(sizeof(t_dir_entry))) == NULL)
		return (NULL);
	ft_memset(input, 0, sizeof(t_dir_entry));
	input->path = ft_dstrcat(NULL, av);
	input->fname = ft_dstrcat(NULL, av);
	if (!input->path || !input->fname)
		return (NULL);
	input->next = NULL;
	return (input);
}

int			file_vane(t_dir_entry *e, t_exec_flags f)
{
	struct stat	tmp;

	if (lstat(e->fname, &e->stat) == -1)
		return (-1);
	if (S_IFLNK & e->stat.st_mode && !ISSET(f, L))
	{
		if (stat(e->fname, &tmp) == 0)
		{
			if (new_hope(e) < 0)
				return (-1);
		}
		errno = 0;
	}
	return (0);
}
