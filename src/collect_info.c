/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   collect_info.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:10:45 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:11:31 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		update(t_dir_entry **arr, ssize_t len, t_exec_flags f)
{
	while (len--)
	{
		if (S_ISLNK(arr[len]->stat.st_mode))
		{
			if (ISSET(f, L))
			{
				if ((arr[len]->fname = get_sym(arr[len])) == NULL)
					return (-1);
			}
			else if (f & INPUT)
				if (new_hope(arr[len]) < 0)
					return (-1);
		}
		if (S_ISDIR(arr[len]->stat.st_mode))
		{
			if (*arr[len]->path != '/' || ft_strlen(arr[len]->path) > 1)
				if ((arr[len]->path = ft_dstrcat(arr[len]->path, "/")) == NULL)
					return (-1);
		}
	}
	return (0);
}

int		gen_info(t_dir_entry **arr, ssize_t len, const t_exec_flags f)
{
	int		i;

	i = -1;
	if (ISSET(f, L) || ISSET(f, T) || ISSET(f, BIG_R)
		|| ISSET(f, S))
	{
		if (cast_lstat(arr, len) < 0)
			return (-1);
		if (ISSET(f, L))
			while (++i < len)
				if ((get_ugid(arr[i], f) + get_rwx(arr[i])
							+ get_time(arr[i], f)) < 0)
					return (-1);
	}
	if (update(arr, len, f) < 0)
		return (-1);
	sort_arbiter(arr, len, f);
	return (0);
}
