/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/15 22:05:48 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:52:13 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	*print_error(char *ent)
{
	char *str;

	str = ent;
	ft_dprintf(2, "ls: %s: %s\n", str, strerror(errno));
	errno = 0;
	g_status = 1;
	return (NULL);
}

void	init_exit(int e)
{
	perror("");
	exit(e);
}
