/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   format_output_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:18:09 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:37:19 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

size_t	multicol(t_direnv *d, t_exec_flags f)
{
	struct winsize	ws;
	size_t			cs;

	if (ISSET(f, ONE))
		return (1);
	if (ioctl(1, TIOCGWINSZ, &ws) < 0)
		ws.ws_col = 80;
	if (d->fname_f + 8 - (d->fname_f % 8) <= ws.ws_col)
		d->fname_f += 8 - (d->fname_f % 8);
	else
		return (1);
	errno = 0;
	cs = ws.ws_col / d->fname_f;
	return (cs);
}

void	aux_params(ssize_t len, t_dir_entry **arr, t_direnv *d, t_exec_flags f)
{
	if (len > 0)
		while (len--)
		{
			if (ISSET(f, L))
			{
				if (arr[len]->stat.st_nlink > d->link_f)
					d->link_f = arr[len]->stat.st_nlink;
				if ((int)ft_strlen(arr[len]->uname) > d->uname_f)
					d->uname_f = ft_strlen(arr[len]->uname);
				if ((int)ft_strlen(arr[len]->gname) > d->gname_f)
					d->gname_f = ft_strlen(arr[len]->gname);
				if (arr[len]->stat.st_size > d->bytes_f)
					d->bytes_f = arr[len]->stat.st_size;
				if (MAJOR(arr[len]->stat.st_rdev) > d->maj_f)
					d->maj_f = MAJOR(arr[len]->stat.st_rdev);
				if (MINOR(arr[len]->stat.st_rdev) > d->min_f)
					d->min_f = MINOR(arr[len]->stat.st_rdev);
				d->chunks += arr[len]->stat.st_blocks;
			}
			if ((int)ft_strlen(arr[len]->fname) > d->fname_f)
				d->fname_f = ft_strlen(arr[len]->fname);
		}
}

void	sort_arbiter(t_dir_entry **arr, ssize_t len, t_exec_flags f)
{
	if (ISSET(f, F))
	{
		reverse(arr, len);
		return ;
	}
	if (ISSET(f, S))
		q_sort(arr, len, SIZE);
	else if (ISSET(f, T))
	{
		if (ISSET(f, U))
			q_sort(arr, len, UTIME);
		else
			q_sort(arr, len, TIME);
	}
	else
		q_sort(arr, len, ALPHA);
	if (ISSET(f, R))
		reverse(arr, len);
}

void	kill_dir(char **av, int ac)
{
	while (ac--)
		if (!ft_strcmp(av[ac], ""))
		{
			ft_dprintf(2, "ls: fts_open: No such file or directory\n");
			exit(1);
		}
}

void	q_sort(t_dir_entry **arr, size_t len, t_mode mode)
{
	size_t	(*part)(t_dir_entry **, size_t, size_t);

	if (len > 1)
	{
		part = partition_alpha;
		if (mode == TIME)
			part = partition_time;
		if (mode == UTIME)
			part = partition_utime;
		if (mode == SIZE)
			part = partition_size;
		q_sort_generic(arr, 0, len - 1, part);
	}
}
