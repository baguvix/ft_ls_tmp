/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 19:10:52 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:52:26 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		print_dirs(t_dir_entry *dirs, t_exec_flags flags)
{
	t_dir_entry **arr;
	ssize_t		len;
	ssize_t		i;
	int			e;

	e = 0;
	if ((len = list_to_arr(dirs, &arr)) < 0)
		e = 1;
	if (!e && gen_info(arr, len, flags) < 0)
		e = 1;
	i = 0;
	while (!e && i < len)
	{
		if (process_dir(arr[i++], flags) < 0)
			e = 1;
		if (!e && i < len)
			write(1, "\n", 1);
	}
	free(arr);
	if (e)
		return (-1);
	return (0);
}

int		inp_files(t_dir_entry *f, t_exec_flags flags)
{
	t_dir_entry **arr;
	ssize_t		len;
	int			e;
	t_direnv	direnv;

	e = 0;
	if ((len = list_to_arr(f, &arr)) < 0)
		e = 1;
	if (!e && gen_info(arr, len, flags |= INPUT) < 0)
		e = 1;
	if (!e)
		direnv.line_size = get_direnv(arr, len,
				(t_direnv *)ft_memset(&direnv, 0, sizeof(t_direnv)), flags);
	if (!e && print_info(arr, len, &direnv, flags) < 0)
		e = 1;
	free(arr);
	if (e)
		return (-1);
	return (0);
}

int		main(int argc, char **argv)
{
	t_exec_flags	flags;
	t_dir_entry		*files;
	t_dir_entry		*dirs;
	int				e;

	e = 0;
	flags = 0;
	dirs = NULL;
	files = read_args(argc, argv, &flags);
	e = ((!ISSET(flags, D) && exclude_dirs(&files, &dirs, flags) < 0) ? 1 : e);
	if (!e && files)
	{
		if (inp_files(files, flags) < 0)
			e = 1;
		if (!e && dirs)
			write(1, "\n", 1);
	}
	clear_list(&files);
	if (!e && dirs)
		if (print_dirs(dirs, flags) < 0)
			e = 1;
	clear_list(&dirs);
	if (e)
		init_exit(2);
	exit(g_status);
}
