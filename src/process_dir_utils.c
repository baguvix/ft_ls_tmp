/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_dir_utils.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 01:40:27 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:40:42 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		exit_condition(void)
{
	if (errno == 0 || errno == EPERM
		|| errno == ENOENT || errno == EACCES)
		return (0);
	return (-1);
}

void	entry_swap(t_dir_entry **arr, size_t i, size_t j)
{
	t_dir_entry	*tmp;

	tmp = arr[i];
	arr[i] = arr[j];
	arr[j] = tmp;
}
