/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:34:52 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:36:00 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_dir_entry	*read_args(int ac, char **av, t_exec_flags *flags)
{
	int			i;
	int			tmp;
	t_dir_entry	*head;
	t_dir_entry	*input;

	head = NULL;
	i = read_flags(ac, av, flags);
	kill_dir(av, ac);
	tmp = i;
	sort_inp(av, i, ac - 1);
	while (i < ac)
	{
		if (skip_check(av[i]) && ++i)
			continue;
		if (errno || (input = init_input(av[i++])) == NULL)
		{
			clear_list(&head);
			init_exit(2);
		}
		input->next = head;
		head = input;
	}
	if (head == NULL && tmp == i && (head = init_input(".")) == NULL)
		init_exit(2);
	return (head);
}

size_t		part(char **av, size_t low, size_t high)
{
	char	*pivot;
	char	*tmp;
	size_t	i;
	size_t	j;

	pivot = av[high];
	i = low;
	j = low;
	while (j < high)
	{
		if (ft_strcmp(av[i], pivot) <= 0)
		{
			tmp = av[i];
			av[i++] = av[j];
			av[j] = tmp;
		}
		++j;
	}
	tmp = av[i];
	av[i] = av[high];
	av[high] = tmp;
	return (i);
}

void		sort_inp(char **av, size_t low, size_t high)
{
	size_t	p;

	if (low < high)
	{
		p = part(av, low, high);
		if (p > 1)
			sort_inp(av, low, p - 1);
		if (p < high)
			sort_inp(av, p + 1, high);
	}
}

int			read_flags(int ac, char **av, t_exec_flags *flags)
{
	int i;

	i = 1;
	if (isatty(1))
		set_bit(flags, BIG_C);
	else
		set_bit(flags, ONE);
	errno = 0;
	while (i < ac && *av[i] == '-' && *av[i]
			&& ft_strcmp("--", av[i])
			&& ft_strlen(av[i]) > 1)
		get_opt(av[i++], flags);
	if (i < ac && av[i][1] == '-')
		++i;
	if (ac - i > 1)
		*flags |= PRINT_DIR_NAME;
	if (ISSET(*flags, F))
		set_bit(flags, A);
	if (ISSET(*flags, G))
		set_bit(flags, L);
	if (ISSET(*flags, A))
		unset_bit(flags, BIG_A);
	return (i);
}

int			exclude_dirs(t_dir_entry **files,
				t_dir_entry **head, t_exec_flags f)
{
	t_dir_entry	*prev;
	t_dir_entry	*curr;
	t_dir_entry	*tmp;

	prev = NULL;
	curr = *files;
	while (curr)
	{
		if (file_vane(curr, f) < 0)
			return (-1);
		tmp = curr->next;
		if (S_IFDIR & curr->stat.st_mode)
		{
			if (prev != NULL)
				prev->next = curr->next;
			else
				*files = (*files)->next;
			curr->next = *head;
			*head = curr;
		}
		else
			prev = curr;
		curr = tmp;
	}
	return (0);
}
