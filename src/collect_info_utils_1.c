/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   collect_info_utils_1.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:12:10 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:12:25 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	reverse(t_dir_entry **arr, ssize_t len)
{
	t_dir_entry	*tmp;
	size_t		i;

	i = 0;
	--len;
	while (i < (unsigned long)len)
	{
		tmp = arr[i];
		arr[i] = arr[len];
		arr[len] = tmp;
		++i;
		--len;
	}
}

int		cast_lstat(t_dir_entry **arr, ssize_t len)
{
	while (len--)
		if (lstat(arr[len]->path, &arr[len]->stat) < 0)
			return (-1);
	return (0);
}

int		get_time(t_dir_entry *entry, t_exec_flags f)
{
	time_t	t;
	time_t	c;
	char	*year;

	t = entry->stat.st_mtimespec.tv_sec;
	if (ISSET(f, U))
		t = entry->stat.st_atimespec.tv_sec;
	entry->time = ft_dstrcat(NULL, ft_strchr(ctime(&t), ' ') + 1);
	if (entry->time == NULL)
		return (-1);
	time(&c);
	if (c - t >= 15768000 || t - c >= 15768000)
	{
		year = ft_strrchr(entry->time, ' ');
		ft_memcpy(entry->time + 7, year, ft_strlen(year));
		entry->time[6 + ft_strlen(year)] = '\0';
		return (0);
	}
	entry->time[12] = '\0';
	return (0);
}

int		new_hope(t_dir_entry *e)
{
	if (stat(e->path, &e->stat) < 0)
	{
		if (errno == ENOENT)
		{
			errno = 0;
			return (0);
		}
		return (-1);
	}
	return (0);
}

char	*get_sym(t_dir_entry *e)
{
	char	*tmp;
	char	buf[BUFF_SIZE + 1];
	ssize_t	rc;

	rc = readlink(e->path, buf, BUFF_SIZE);
	if (rc == -1)
		return (NULL);
	buf[rc] = '\0';
	tmp = ft_dstrcat(e->fname, " -> ");
	if (tmp == NULL)
		return (NULL);
	tmp = ft_dstrcat(tmp, buf);
	return (tmp);
}
