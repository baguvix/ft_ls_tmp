/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   q_sort.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:39:44 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:39:46 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ls.h>

void	q_sort_generic(t_dir_entry **arr, size_t low, size_t high,
		size_t (*part)(t_dir_entry **, size_t, size_t))
{
	size_t	p;

	if (low < high)
	{
		p = part(arr, low, high);
		if (p > 1)
			q_sort_generic(arr, low, p - 1, part);
		if (p < high)
			q_sort_generic(arr, p + 1, high, part);
	}
}

size_t	partition_utime(t_dir_entry **arr, size_t low, size_t high)
{
	t_dir_entry	*pivot;
	size_t		i;
	size_t		j;

	pivot = arr[high];
	i = low;
	j = low;
	while (j < high)
	{
		if (arr[j]->stat.st_atimespec.tv_sec > pivot->stat.st_atimespec.tv_sec)
		{
			entry_swap(arr, i, j);
			++i;
		}
		if (arr[j]->stat.st_atimespec.tv_sec == pivot->stat.st_atimespec.tv_sec
				&& ft_strcmp(arr[j]->fname, pivot->fname) <= 0)
		{
			entry_swap(arr, i, j);
			++i;
		}
		++j;
	}
	entry_swap(arr, i, high);
	return (i);
}

size_t	partition_time(t_dir_entry **arr, size_t low, size_t high)
{
	t_dir_entry	*pivot;
	size_t		i;
	size_t		j;

	pivot = arr[high];
	i = low;
	j = low;
	while (j < high)
	{
		if (arr[j]->stat.st_mtimespec.tv_sec > pivot->stat.st_mtimespec.tv_sec)
		{
			entry_swap(arr, i, j);
			++i;
		}
		if (arr[j]->stat.st_mtimespec.tv_sec == pivot->stat.st_mtimespec.tv_sec
				&& ft_strcmp(arr[j]->fname, pivot->fname) <= 0)
		{
			entry_swap(arr, i, j);
			++i;
		}
		++j;
	}
	entry_swap(arr, i, high);
	return (i);
}

size_t	partition_alpha(t_dir_entry **arr, size_t low, size_t high)
{
	t_dir_entry	*pivot;
	size_t		i;
	size_t		j;

	pivot = arr[high];
	i = low;
	j = low;
	while (j < high)
	{
		if (ft_strcmp(arr[j]->fname, pivot->fname) <= 0)
		{
			entry_swap(arr, i, j);
			++i;
		}
		++j;
	}
	entry_swap(arr, i, high);
	return (i);
}

size_t	partition_size(t_dir_entry **arr, size_t low, size_t high)
{
	t_dir_entry	*pivot;
	size_t		i;
	size_t		j;

	pivot = arr[high];
	i = low;
	j = low;
	while (j < high)
	{
		if (arr[j]->stat.st_size > pivot->stat.st_size)
		{
			entry_swap(arr, i, j);
			++i;
		}
		if (arr[j]->stat.st_size == pivot->stat.st_size
				&& ft_strcmp(arr[j]->fname, pivot->fname) <= 0)
		{
			entry_swap(arr, i, j);
			++i;
		}
		++j;
	}
	entry_swap(arr, i, high);
	return (i);
}
