/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   format_output.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 02:25:11 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 03:12:56 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		buffer_mash(t_dir_entry **arr, ssize_t len, t_direnv *d)
{
	ssize_t	i;
	int		r;

	i = -1;
	while (++i < len)
	{
		r = arr[i]->rwx[0] == 'c' || arr[i]->rwx[0] == 'b';
		ft_printf("%-12s%*d %-*s%s%-*s  %*.*d%.*s%*s%*lld %s %s\n",
			arr[i]->rwx, (int)d->link_f, arr[i]->stat.st_nlink, d->uname_f,
			arr[i]->uname, (ISSET(d->f, G) ? "" : "  "),
			d->gname_f, arr[i]->gname, (r ? d->maj_f : 0),
			(r ? 1 : 0), MAJOR(arr[i]->stat.st_rdev),
			(r ? 2 : 0), (r ? ", " : "  "),
			(d->min_f && !(r) ? d->min_f + (int)d->bytes_f + 1 : 0), "",
			(d->min_f ? d->min_f : (int)d->bytes_f),
			(r ? MINOR(arr[i]->stat.st_rdev) : arr[i]->stat.st_size),
			arr[i]->time,
			arr[i]->fname);
	}
	return (0);
}

void	print_k_tabs(int k)
{
	k = (k % 8 > 0 ? 1 : 0) + k / 8;
	while (k--)
		write(1, "\t", 1);
}

int		col_form(t_dir_entry **arr, ssize_t len, t_direnv *d)
{
	int	i;
	int j;
	int h;

	i = 0;
	h = 1;
	if (len > d->line_size)
		h = len / d->line_size + (len % d->line_size ? 1 : 0);
	while (i < h)
	{
		j = 0;
		while (j < d->line_size)
		{
			d->maj_f = (i + j * h < len ? ft_strlen(arr[i + j * h]->fname) : 0);
			ft_printf("%-*s", d->maj_f, (i + j * h < len ?
						arr[i + j * h]->fname : ""));
			if (d->maj_f > 0 && j + 1 != d->line_size && i + (j + 1) * h < len)
				print_k_tabs(d->fname_f - d->maj_f);
			if (++j == d->line_size)
				write(1, "\n", 1);
		}
		++i;
	}
	return (0);
}

int		print_info(t_dir_entry **arr, ssize_t len,
					t_direnv *env, t_exec_flags flags)
{
	if (ISSET(flags, L))
	{
		if (buffer_mash(arr, len, env) < 0)
			return (-1);
	}
	else if (col_form(arr, len, env) < 0)
		return (-1);
	return (0);
}

size_t	get_direnv(t_dir_entry **arr, ssize_t len, t_direnv *d, t_exec_flags f)
{
	d->f = f;
	aux_params(len, arr, d, f);
	if (ISSET(f, L))
	{
		if (d->min_f > 0)
		{
			d->min_f = nofd(d->min_f, 10);
			d->min_f = d->min_f > 2 ? d->min_f : 3;
		}
		d->maj_f = nofd(d->maj_f, 10) + 1;
		d->maj_f = d->maj_f > 2 ? d->maj_f : 3;
		d->bytes_f = nofd(d->bytes_f, 10);
		d->link_f = nofd(d->link_f, 10);
		return (d->link_f + d->uname_f + d->gname_f
				+ d->bytes_f + d->fname_f + 32);
	}
	return (multicol(d, f));
}
