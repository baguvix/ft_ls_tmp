/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   queue.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 18:38:02 by orhaegar          #+#    #+#             */
/*   Updated: 2019/09/25 02:35:39 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ls.h>

int			enqueue(t_queue **q, t_dir_entry *data)
{
	t_queue	*new;

	if ((new = (t_queue *)malloc(sizeof(t_queue))) == NULL)
		return (-1);
	new->data = data;
	new->next = *q;
	*q = new;
	return (0);
}

t_dir_entry	*dequeue(t_queue **q)
{
	t_queue		**curr;
	t_queue		*tmp;
	t_dir_entry	*data;

	if (*q == NULL)
		return (NULL);
	tmp = *q;
	curr = q;
	while (tmp->next)
	{
		curr = &tmp->next;
		tmp = tmp->next;
	}
	data = tmp->data;
	free(tmp);
	*curr = NULL;
	return (data);
}

int			del_queue(t_queue **q)
{
	t_queue		*bye;

	while (*q)
	{
		bye = (*q)->next;
		free(*q);
		*q = bye;
	}
	return (-1);
}
